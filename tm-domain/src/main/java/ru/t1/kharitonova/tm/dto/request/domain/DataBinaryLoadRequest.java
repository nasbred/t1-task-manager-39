package ru.t1.kharitonova.tm.dto.request.domain;

import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;

public final class DataBinaryLoadRequest extends AbstractUserRequest {

    public DataBinaryLoadRequest(@Nullable final String token) {
        super(token);
    }

}
