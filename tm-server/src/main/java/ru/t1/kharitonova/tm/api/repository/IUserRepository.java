package ru.t1.kharitonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.exception.user.UserNotFoundException;
import ru.t1.kharitonova.tm.model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password_hash, email, last_name, first_name, middle_name, role, locked) " +
            "VALUES(#{id}, #{login}, #{passwordHash}, #{email}, #{lastName}, #{firstName}, #{middleName}, " +
            "#{role}, #{locked});")
    void add(@NotNull User user);

    default Collection<User> addAll(@NotNull final Collection<User> users) {
        @NotNull List<User> result = new ArrayList<>();
        for (@NotNull final User user : users) {
            add(user);
            result.add(user);
        }
        return result;
    }

    default boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Select("SELECT COUNT(1) FROM tm_user;")
    int getSize();

    default User remove(@Nullable final User model) {
        if (model == null) throw new UserNotFoundException();
        removeOneById(model.getId());
        return model;
    }

    @Delete("DELETE FROM tm_user WHERE id = #{id};")
    void removeOneById(@NotNull @Param("id") String id);

    default User removeOneByIndex(@NotNull final Integer index) {
        @Nullable final User user = findOneByIndex(index);
        if (user == null) return null;
        remove(user);
        return user;
    }

    @Delete("TRUNCATE TABLE tm_user;")
    void removeAll();

    default void removeAll(@NotNull List<User> users) {
        for (@NotNull final User user : users) remove(user);
    }

    @Select("SELECT * FROM tm_user;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @NotNull
    List<User> findAll();

    @Select("SELECT * FROM tm_user ORDER BY #{getSortType(comparator)};")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @NotNull
    List<User> findAllWithComparator(@NotNull @Param("comparator") Comparator<User> comparator);

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @Nullable
    User findOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_user OFFSET #{index};")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    User findOneByIndex(@Param("index") @NotNull Integer index);

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @Nullable
    User findByLogin(@NotNull @Param("login") String login);

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @Nullable
    User findByEmail(@NotNull @Param("email") String email);

    default Collection<User> set(@NotNull final Collection<User> users) {
        removeAll();
        return addAll(users);
    }

    @Update("UPDATE tm_user SET password_hash = #{passwordHash} WHERE id = #{id};")
    void setPassword(@Nullable @Param("id") String id, @NotNull @Param("passwordHash") String password);

    @Update("UPDATE tm_user SET last_name = #{lastName}, first_name = #{firstName}, middle_name = #{middleName} " +
            "WHERE id = #{id};")
    void updateUser(@Nullable @Param("id") String id,
                    @Nullable @Param("firstName") String firstName,
                    @Nullable @Param("lastName") String lastName,
                    @Nullable @Param("middleName") String middleName);

    @Update("UPDATE tm_user SET locked = true WHERE id = #{id};")
    void lockUserById(@Nullable @Param("id") String id);

    @Update("UPDATE tm_user SET locked = false WHERE id = #{id};")
    void unLockUserById(@Nullable @Param("id") String id);

}
