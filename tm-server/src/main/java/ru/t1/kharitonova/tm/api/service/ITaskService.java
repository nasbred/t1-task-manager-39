package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void add(@NotNull Task model);

    void add(@Nullable String userId, @NotNull Task model);

    @NotNull
    Collection<Task> addAll(@NotNull Collection<Task> models);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Collection<Task> set(@NotNull Collection<Task> models);

    @Nullable
    Task findOneById(@NotNull String id);

    @Nullable
    Task findOneByIdAndUserId(@NotNull String user_id, @NotNull String id);

    @Nullable
    Task findOneByIndex(@NotNull Integer index);

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAll(@NotNull Comparator<Task> comparator);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @NotNull
    List<Task> findAll(@NotNull String userId, @Nullable Comparator<Task> comparator);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    int getSize();

    int getSize(@NotNull String userId);

    boolean existsById(@NotNull String id);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task remove(@Nullable Task model);

    @Nullable
    Task removeOne(@Nullable String userId, @Nullable Task model);

    void removeOneById(@NotNull String id);

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@NotNull Integer index);

    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAll(@NotNull List<Task> models);

    void removeAllByUserId(@NotNull String userId);

    @Nullable
    Task update(@NotNull Task model);

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
