package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void add(@NotNull Project model);

    void add(@Nullable String userId, @NotNull Project model);

    @NotNull
    Collection<Project> addAll(@NotNull Collection<Project> models);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Collection<Project> set(@NotNull Collection<Project> models);

    @Nullable
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @Nullable
    List<Project> findAll(@NotNull Comparator<Project> comparator);

    @NotNull
    List<Project> findAll(@NotNull String userId, @Nullable Comparator<Project> comparator);

    @Nullable
    Project findOneById(@NotNull String id);

    @Nullable
    Project findOneByIdAndUserId(@NotNull String user_id, @NotNull String id);

    @Nullable
    Project findOneByIndex(@NotNull Integer index);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize();

    int getSize(@NotNull String userId);

    boolean existsById(@NotNull String id);

    boolean existsById(@NotNull String userId, @NotNull String id);

    void remove(@Nullable Project model);

    void removeOneById(@NotNull String id);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@NotNull Integer index);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAll(@NotNull List<Project> models);

    void removeAllByUserId(@NotNull String userId);

    void update(@NotNull Project model);

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
