package ru.t1.kharitonova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.ISessionRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.ISessionService;
import ru.t1.kharitonova.tm.exception.entity.ModelNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.model.Session;

public class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session create(@Nullable final Session session) {
        if (session == null) throw new ModelNotFoundException();
        @Nullable final Session result;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.add(session);
            result = repository.findOneById(session.getId());
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findOneByUserId(userId);
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.getSize();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
